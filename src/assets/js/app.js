feather.replace();

$(document).ready(function() {
    $(".search-header input").on("focus", function() {
        if (this.setSelectionRange) {
          var len = this.value.length * 2;
          this.setSelectionRange(len, len);
        } else {
          this.value = this.value;
        }
    }).focus();

    $(".search-header input").on("input", function() { 
        $('#searchHeaderButton').show('slow');
        //return false;
        if( !$(this).val() ) {
            $('#searchHeaderButton').hide('slow'); 
        }
    });

});
